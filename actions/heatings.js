const request = require('request')
const cron = require('node-cron')
const moment = require('moment')

var ipAddress = '192.168.1.31'
var _jobs = []
var _logs = []

class Heatings {
    // List of cron jobs

    static logs() { return _logs }

    static emptyLogs() {
        _logs.length = 0
    } 
    static jobs() { return _jobs }
    
    static addJob(job) {
        var task = cron.schedule(job.rule, () => {
            Heatings.change(job.heating, job.mode, job.duration, job.unit, () => {
                _logs.push(`${moment().format('DD-MM-YYYY hh:mm:ss')}: /heatings/${job.heating} trigger ${job.rule}`)
            })
        })
        job.task = task
        _jobs.push(job)
    }

    static getAll(cb) {
        request(
            `http://${ipAddress}/api/heatings/all`, 
            (error, response, body) => {
            cb( error ? [] : JSON.parse(body))
        })
    }

    static get(heating, cb) {
        request(
            `http://${ipAddress}/api/heatings/${heating}`, 
            (error, response, body) => {
            cb( error ? {} : JSON.parse(body))
        })        
    }

    static change(heating, mode, duration, unit, cb) {
        request.post(
            `http://${ipAddress}/api/heatings/${heating}`,
            { form: { 
                mode: mode || 2,
                duration: duration || 60,
                unit: unit || 1
            } }, (error, request, body) => {
                _logs.push(`${moment().format('DD-MM-YYYY hh:mm:ss')}: /heatings/${heating} change to ${mode} for ${duration} ${['hrs', 'mins', 'secs'][unit]}`)
                cb( error ? {} : JSON.parse(body))
         })
    }

    static setIp(ip) {
        ipAddress = ip
    }

    static server(cb) {
        request.get(`http://${ipAddress}/server`, (error, response, body) => {
            cb(error ? {} : JSON.parse(body))
        })
    }
}

module.exports = Heatings
