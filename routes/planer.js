var express = require('express')
var router = express.Router()
var Heatings = require('../actions/heatings') 

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('planer', { jobs: Heatings.jobs(), logs: Heatings.logs() })
})

router.post('/', function(req, res, next) {
  let heating = req.body.heating
  if (req.body.jobs == 'add') {
    Heatings.addJob({
      heating: req.body.heating,
      rule: req.body.rule || '* * * * *',
      mode: req.body.mode || '1',
      duration: req.body.duration || 60,
      unit: req.body.unit || 1
    })
  }
  
  if (req.body.logs == 'del') {
    Heatings.emptyLogs()
  }
  if (heating == undefined) {
    res.redirect('/plan')
  } else {
    Heatings.get(heating, (h) => {
      h.id = heating
      res.render('planer', { heating: h, jobs: Heatings.jobs() })
    })
  }
})


module.exports = router