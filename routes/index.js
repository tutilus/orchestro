var express = require('express')
var router = express.Router()
var Heatings = require('../actions/heatings') 
var moment = require('moment')

moment.locale('fr')

/* GET home page. */
router.get('/', function(req, res, next) {
  Heatings.getAll(function(heatings) {
    heatings.forEach(h => {
      if (h.startedAt) {
        h.leftTime = moment(h.startedAt)
      }        
    });
    res.render('index', { heatings: heatings });
  })
})

router.post('/', function(req, res, next) {
  const mode = req.body.mode
  const heating = req.body.heating
  const duration = req.body.duration
  const unit = req.body.unit
  Heatings.change(heating, mode, duration, unit, () => {
    res.redirect('/')
  })
})

module.exports = router;
